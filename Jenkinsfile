pipeline {
  agent any
  parameters {
    text(name: 'MY_PUBLIC_IP', defaultValue: '143.44.192.4/32', description: 'The public IP where SSH is allowed.')
    text(name: 'JENKINS_IP', defaultValue: '170.64.170.203/32', description: 'The public IP where SSH is allowed.')
    text(name: 'CONTAINER_NAMESPACE', defaultValue: 'sheenismhaellim', description: 'The namespace part of the FQN (Fully Qualified Name) of a Image.')
    text(name: 'CONTAINER_REPO_NAME', defaultValue: 'itfellas', description: 'The repository name of the FQN (Fully Qualified Name) of a Image.')
    text(name: 'MYSQL_USER', defaultValue: 'petclinic', description: 'The db admin user to assign to the DB and the sql user Petclinic will use to connect to the db.')
    text(name: 'MYSQL_PASS', defaultValue: 'petclinic', description: 'The db admin password for the db admin user.')
    text(name: 'SSH_KEY', defaultValue: 'petclinic', description: 'The id of the SSH key in AWS to associate with EC2 SSH access.')
  }

  tools {
    maven 'Maven-3.9.1'
    jdk 'jdk-21-GA'
  }

  stages {
    stage("Initialise Env Vars") {
      steps {
        script {
          def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
          env.VERSION = matcher[0][1]
          env.TAG_NAME = "${env.VERSION}-${env.BUILD_NUMBER}"
          env.IMAGE_NAME = "docker.io/${params.CONTAINER_NAMESPACE}/${params.CONTAINER_REPO_NAME}:${env.TAG_NAME}"

          env.MYSQL_USER = "${params.MYSQL_USER}"
          env.MYSQL_PASS = "${params.MYSQL_PASS}"
          env.SSH_KEY = "${params.SSH_KEY}"
        }
      }
    }    

    stage("Build artifact for Java Maven Application") {
      steps {
        script {
          sh "./mvnw clean"
          sh "./mvnw package -Dmaven.test.skip.exec -Dspring-boot.run.profiles=mysql"
        }
      }
    }

    stage("Build and Push Docker image to AWS ECR") {
      steps {
        script {
          sh "envsubst < Dockerfile-template > Dockerfile"
          sh "docker build -t ${env.IMAGE_NAME} ."
          sh "rm Dockerfile"

          withCredentials([usernamePassword(credentialsId: 'docker-hub-credentials', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
            sh "echo $PWD | docker login -u $USER --password-stdin docker.io"
            sh "docker push ${env.IMAGE_NAME}"
            sh "docker logout docker.io"
          }
        }
      }
    }

    stage("Provision EC2") {
      environment {
        MY_PUBLIC_IP="${params.MY_PUBLIC_IP}"
        JENKINS_IP="${params.JENKINS_IP}"
      }
      steps {
        script {
          dir("terraform") {
            sh "envsubst < terraform-template.tfvars > terraform.tfvars"
            sh "terraform init -migrate-state"
            sh "terraform apply -var-file terraform.tfvars -auto-approve"

            env.EC2_PUBLIC_IP = sh(
              script: 'terraform output ec2_public_ip',
              returnStdout: true
            ).trim()

            // wait for ec2 to be ready
            sh 'sleep 3m'

            sshagent(['petclinic']) {
              sh "ssh -o StrictHostKeyChecking=no ubuntu@${env.EC2_PUBLIC_IP} 'mkdir -p /home/ubuntu/.docker'"
              sh "scp -o StrictHostKeyChecking=no ~/.docker/config.json ubuntu@${env.EC2_PUBLIC_IP}:/home/ubuntu/.docker/config.json"
              sh "ssh -o StrictHostKeyChecking=no ubuntu@${env.EC2_PUBLIC_IP} 'sudo systemctl restart docker'"
            }
          }
        }
      }
    }

    stage("Deploy application on EC2") {
      steps {
        script {
          sh "envsubst < docker-compose-template.yml > docker-compose.yml"

          sshagent(['petclinic']) {
            sh "scp -o StrictHostKeyChecking=no docker-compose.yml ubuntu@${env.EC2_PUBLIC_IP}:/home/ubuntu/docker-compose.yml"
            sh "ssh -o StrictHostKeyChecking=no ubuntu@${env.EC2_PUBLIC_IP} 'docker compose -f /home/ubuntu/docker-compose.yml up --detach'"
          }

          sh "rm docker-compose.yml"
        }
      }
    }
  }
}