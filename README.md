# Introduction

This project is a sample implementation of all the knowledge learned in the DevOps Bootcamp from Techworld with Nana for Module 12.

NOTE: This Capstone Project deploys the same application (spring-petclinic) as Capstone Project 1 (https://gitlab.com/twwnana/capstoneprojects/capstone-proj01).

<img width="650" alt="petclinic-screenshot" src="https://lh3.googleusercontent.com/DP5IAoIV-hLLCUtQ-57Hjo2Vi9YuRp1J9HCVTiX35wii_GYE_ZRQdoMcw5Cen39pUU9Zi6UfAF0hN-JJ6Pt7D0TFs28JeCkqtf746VlklsW54Tu_BSiwz2XRAyf2JN5DYg=w544">

Bootcamp Chapter 12 Git Repo: 
- git@gitlab.com:twwnana/terraform/lecture.git

# Requirement before running the terraform module
- Create the Key pair object in AWS. This can be use to SSH to the EC2 instance for troubleshooting should that be needed.


# Pipeline parameters
There are several pipeline parameters that user should enter.
- MY_PUBLIC_IP - This will be the public IP where SSH client connections will be allowed from (used together with SSH Key). This must be in the CIDR format (like 999.999.999.999/32).
- JENKINS_IP - Is the IP address of the Jenkins server. This must be in the CIDR format (like 999.999.999.999/32).
- CONTAINER_NAMESPACE - The namespace part of the docker hub FQIM (Fully Qualified Image Name). <NAME_SPACE>/<REPO_NAME>:<TAG_NAME>
- CONTAINER_REPO_NAME - The namespace part of the docker hub FQIM (Fully Qualified Image Name). <NAME_SPACE>/<REPO_NAME>:<TAG_NAME>
- MYSQL-USER - The sql user to assign to the petclinic database, this is also the mysql that the petclinic app will use to connect to the database.
- MYSQL_PASS - The password for the sql user.
- SSH_KEY - the key pair name in AWS that you created. (This must be created before the pipeline is ran)

# Testing the application Output
Once the pipeline is ran and the appropriate parameters provided, you may check the EC2 IP address and access the app in the form on EC2_PUBLIC_IP:8080.
The IP visiting the site must either be the value of JENKINS_IP or MY_PUBLIC_IP.
http://ec2_public_ip:8080

## Tech Stack
**Infrastructure:** AWS ECR, AWS EKS

**Technologies:** Containerization (Docker, Docker Compose and AWS ECR), CICD Pipeline (Jenkins), Git (Code Versioning), Maven (application), Terraform (AWS EKS provisioning)

# Credits
The original spring-petclinic can be found at https://github.com/spring-projects/spring-petclinic